const helpers = require('./helpers');
const webpack = require('webpack');
const validator = require('webpack-validator');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

//__dirname - sciezka do folderu gdzie jest plik
var path = require('path');

module.exports  = validator({
    entry: {
        app: './src/app/app.js',
        vendor: './src/vendor.js'
    },
    output: {
        //filename: 'app.bundle.js',
        filename: '[name].[chunkhash].js',
        //path: './build'
        //path: 'c:\\dev\\angular\\webpack\\build'
        //path: path.join(__dirname, '..', 'build')
        path: helpers.absolutePath('build')
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'eslint-loader',
                exclude: /(node_modules)/
            },
            {
                test: /\.js$/,
                loader: 'ng-annotate',
                exclude: /(node_modules)/
            },
            {
                test: /\.js$/,
                loader: 'babel',
                exclude: /(node_modules)/,
                query: {
                    presets: 'es2015'
                }
            },
            /*{
                test: /\.scss$/,
                loaders: ['style','css','sass'],
                exclude: /(node_modules)/
            }*/
            {
                 test: /\.scss$/,
                 loader: ExtractTextPlugin.extract('style','css!sass'),
                 exclude: /(node_modules)/
            },
            {
                test: /\.html$/,
                loader: 'raw-loader',
                exclude: [helpers.absolutePath('src/index.html')]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html',
            inject: 'body'
        }),

        new webpack.optimize.CommonsChunkPlugin({
            name: ['vendor','manifest']
        }),

        new CleanWebpackPlugin(['build'],{
            root: helpers.absolutePath(''),
            verbose: true
        }),

        new ExtractTextPlugin('[name].[chunkhash].css')
    ],
    devServer: {
        port: 3000,
        host: 'localhost',
        historyApiFallback: true,
        watchOptions: {
            aggregateTimeout: 300
        },
        stats: 'errors-only'
    },
    eslint: {configFile: './config/eslintrc.json'}
});