const webpack = require('webpack');
const config = require('./webpack.config.js');
const webpackMerge = require('webpack-merge');
const definePlugin = require('webpack/lib/DefinePlugin');

module.exports = webpackMerge(config, {
    devtool: 'nsource-map',
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        }),
        new definePlugin({
            ENV: JSON.stringify('production')
        })
    ]
});