const config = require('./webpack.config.js');
const webpackMerge = require('webpack-merge');
const definePlugin = require('webpack/lib/DefinePlugin');

module.exports = webpackMerge(config, {
    debug: true,
    cache: true,
    devtool: 'ncheap-eval-source-map',

    plugins: [
        new definePlugin({
            ENV: JSON.stringify('develop')
        })
    ]
});