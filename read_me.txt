npm install webpack@version
npm install webpack-validator
npm install babel-loader babel-core babel-preset-es2015 --save-dev
npm install eslint-loader --save-dev
npm install eslint --save-dev
npm install html-webpack-plugin --save-dev
npm install webpack-dev-server --save-dev
npm install clean-webpack-plugin --save-dev
npm install extract-text-webpack-plugin --save-dev

//css
npm install style-loader css-loader --save-dev
npm install sass-loader node-sass --save-dev

npm install angular-ui-router --save

npm install raw-loader --save-dev

npm install webpack-merge --save-dev

webpack --config config/webpack.config.js
npm run build



//installation tests
npm install --save-dev
angular-mocks - mokowanie
jasmine-core - framework testowania
karma - test runner
karma-cli -g
karma-chrome-launcher
karma-jasmine
karma-phantomjs-launcher
karma-sourcemap-loader
karma-webpack
phantomjs-prebuilt

//commands
karma-init


