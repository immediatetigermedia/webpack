import angular from 'angular';
// Built by the core Angular team for mocking dependencies
import mocks from 'angular-mocks';
import app from './app/app.js'

let context = require.context('.', true, /\.spec\.js/);

// Get all files, for each file, call the context function
// that will require the file and load it here. Context will
// loop and require those spec files here.
context.keys().forEach(context);

