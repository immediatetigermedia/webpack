class HomeCtrl {
    /*@ngInject*/
    constructor($state, $scope, SearchService) {
        /*this.$state = $state;
        this.$scope = $scope;*/

        angular.extend(this, {
            $state,
            $scope,
            SearchService,
            itemList: []
        });
    }

    onSearchChange(searchText) {
        this.SearchService.search(searchText)
            .then(response => {
                this.itemList = response.map((item) => item.show);
                /*console.log("THIS: ", this.itemList)*/
            })
            .catch(rejectedResponse => console.log(rejectedResponse))
    }

    onItemClick(item) {
        console.log('item: ',item);
        console.log('item id: ',item.id);
        this.$state.go('showDetails', {item, id: item.id})
    }
}

export default HomeCtrl;