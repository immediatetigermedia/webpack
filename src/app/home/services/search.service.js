import TvAPI from './../../helpers/tvAPI.js';

class SearchService extends TvAPI {
    constructor(apiUrl, $http, $q) {
        super(apiUrl);
        angular
            .extend(this, {
                $http,
                $q
            });
    }

    search(term) {
        if (term == 'titanic') {
            return this.$q.reject('we dont like titatnic movies');
        }

        //Create a promise
        let promiseObject = {
            method: 'GET',
            url: this.getApiSearchShowUrl(),
            params: {
                q: term
            }
        };

        /*let promiseObject2 = JSON.parse(JSON.stringify(promiseObject));
        promiseObject2.params.q = 'b';
        let promiseObject3 = JSON.parse(JSON.stringify(promiseObject));
        promiseObject3.params.q = 'c';*/

        /*console.log(promiseObject,promiseObject2, promiseObject3);*/

        /*
        let promiseObject2 = Object.assign({}, promiseObject);
        promiseObject2.params.q = 'b';

        let promiseObject3 = Object.assign({}, promiseObject);
        promiseObject3.params.q = 'c';
        */

        /*let promiseList = [
            this.$http(promiseObject),
            this.$http(promiseObject2),
            this.$http(promiseObject3)
        ];*/

        return this.$http(promiseObject)
        /*return this.$q.all(promiseList)*/
            /*.then(([response, response2, response3]) => response2.data)*/
            .then((response) => response.data)
            .catch((response) => console.log(response));
    }
}

export default angular
    .module('goApp.home.service', [])
    .service('SearchService', SearchService);
