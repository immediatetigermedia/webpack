import HomeCtrl from './home.controller';

describe('home ctrl', () => {

    let $scope, $state, $q, SearchService, homeCtrl;
    beforeEach(() => {

        angular.mock.module('goApp');

        angular.mock.module(($provide) => {
            $provide.service('$state', function () {
                this.go = jasmine.createSpy('go');
            });
        });

        angular.mock.inject((_$controller_, _$q_, _$rootScope_, _$state_, _SearchService_) => {

            $scope = _$rootScope_.$new();
            $state = _$state_;
            SearchService = _SearchService_;
            $q = _$q_;

            homeCtrl = _$controller_('HomeCtrl', {
                $state,
                $scope,
                SearchService
            });

        });
    });

    /*var homeCtrl = {};
        homeCtrl.searchValue = 'test';*/

    //something is fucket up
    it('should exist', () => {
     expect(homeCtrl).toBeDefined();
     });

    /*it('should expose searchValue property', () => {
        expect(HomeCtrl.searchValue).toBeDefined();
    });*/
});
