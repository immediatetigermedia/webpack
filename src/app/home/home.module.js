import HomeCtrl from './home.controller';
require('./../scss/home/home.scss');

const HomeModule = angular
    .module('goApp.home', [])
    .controller('HomeCtrl', HomeCtrl)
    .config(function($stateProvider) {
    $stateProvider
        .state('home', {
            url: '/home',
            template: require('./home.html'),
            controller: HomeCtrl,
            controllerAs: 'home'
        });
});

export default HomeModule;