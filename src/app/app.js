require('./scss/app.scss');

function getModuleName(module) {
    return module.name || module.default.name;
}

let appDependencies = [
    'ui.router',
    'ngMaterial',
    'ngAnimate',
    'ngAria',
    'ngSanitize'
];

let appModule = [

    //Directive
    require('./navbar/navbar.module.js'),

    //Views
    require('./home/home.module.js'),
    require('./colors/colors.module.js'),
    require('./show-details/show-details.module.js'),

    //Testing
    require('./testing/controllers/controllers.module.js'),

    //Services
    require('./home/services/search.service.js')

];

angular
    .module('goApp', appDependencies.concat(appModule.map(getModuleName)))
    /*.controller('homeCtrl', () => {})*/
    .constant('apiUrl','//api.tvmaze.com')
    .config(($urlRouterProvider, $mdThemingProvider) => {
        $urlRouterProvider.otherwise('/home');
        $mdThemingProvider.theme('default')
        .primaryPalette('blue')
        .accentPalette('pink');
    });









