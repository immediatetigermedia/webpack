import ShowDetailsCtrl from './show-details.controller';
import ShowDetailsService from './show-details.service';

//@ngInject
const showDetails = angular
    .module('goApp.showDetails',[])
    .service('ShowDetailsService',ShowDetailsService)
    .config(($stateProvider) => {
        $stateProvider
        .state('showDetails', {
            url: '/showDetails/:id',
            template: require('./show-details.html'),
            controller: ShowDetailsCtrl,
            controllerAs: 'showDetails',
            params: {
                item: null
            }
        });
});
export default showDetails;