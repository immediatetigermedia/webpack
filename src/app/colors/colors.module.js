import ColorsCtrl from './colors.controller';

const ColorsModule = angular.module('goApp.colors',[])
    .config(($stateProvider) => {
    $stateProvider
        .state('colors', {
            url: '/colors',
            template: require('./colors.html'),
            controller: ColorsCtrl,
            controllerAs: 'colors'
        });
});

export default ColorsModule;