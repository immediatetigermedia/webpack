class ColorsCtrl {
    constructor() {
        this.title = "title";
        this.desc = "desc";

        this.errorMsg = false;
        this.successMsg = false;
        this.title = 'Starter Wepack';
        this.desc = function() {
            return 'Building something with webpack and Angular 1';
        };
        this.colors = getColors();
        this.addColor = function(colorName) {
            this.errorMsg = false;
            this.successMsg = false;
            if (colorName == 'red' || colorName == 'blue' || colorName == 'pink' || colorName == 'green' ) {
                this.colors.unshift(colorName);
                this.successMsg = true;
            } else {
                this.errorMsg = true;
            }
        };
        this.deleteColor = function(i) {
            this.colors.splice(i,1);
        };
    }

}

function getColors() {
    var colors = ['red','blue','pink','green'];
    var colorsArr = [];

    for (var i = 0; i < colors.length; i++ ) {
        colorsArr.push(colors[i]);
    }

    if (!!colorsArr.length) {
        return colorsArr;
    }
}

export default ColorsCtrl;
