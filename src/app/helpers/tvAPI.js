class TvAPI {
    constructor(apiUrl) {
        let _apiUrl = apiUrl;
        this.createApiSearchUrl = function() {
            return `${_apiUrl}/search/shows`;
        };
        this.getApiUrl = function() {
            return _apiUrl;
        };
    }

    getApiSearchShowUrl() {
        return this.createApiSearchUrl();
    }

    getApiSearchShow(showId) {
        return this.getApiUrl() + `/shows/${showId}`;
    }
}

export default TvAPI;