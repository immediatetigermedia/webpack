import NavbarDirective from './navbar.directive.js';

const navbarModule = angular
        .module('goApp.navbar',[])
        .directive('navbar', () => new NavbarDirective());

export default navbarModule;
