class NavbarCtrl {
    constructor($scope) {
        angular.extend(this, {
            $scope
        });

        this.envVersion = ENV;
        this.navArr = [];
        this.getList = this.createNavArr();

    }

    createNavArr() {
        let nav = ['home','colors','controllers'];
        return nav;
    }

}
export default NavbarCtrl;
