class ControllersCtrl {
    constructor($state,$scope) {

        angular.extend(this, {
            $scope,
            title: this.getTitle()
        });
    }

    //this.title = this.getTitle()

    getTitle() {
        return 'Controllers';
    }
}

export default ControllersCtrl;