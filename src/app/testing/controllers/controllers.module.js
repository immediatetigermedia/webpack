import ControllersCtrl from './controllers.controller';

const ControllersModule = angular
    .module('goApp.controllers',[])
    .config(function($stateProvider){
        $stateProvider
            .state('controllers', {
                url: '/controllers',
                template: require('./controllers.html'),
                controller: ControllersCtrl,
                controllerAs: 'vm'
            });
    });


export default ControllersModule;